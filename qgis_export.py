#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2023] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Qgis Export - Simple formatting of svg for Qgis
# https://gitlab.com/inklinea/qgis-export
# An Inkscape 1.2.1+ extension
##############################################################################

import inkex

import os, sys

from lxml import etree


class QgisExport(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--qgis_svg_filepath", type=str, dest="qgis_svg_filepath", default='')

        pars.add_argument("--selection_type_radio", type=str, dest="selection_type_radio", default='all')

        pars.add_argument("--fill_color", type=inkex.Color, dest="fill_color", default=True)
        pars.add_argument("--stroke_color", type=inkex.Color, dest="stroke_color", default=True)

        pars.add_argument("--fill_opacity_float", type=float, dest="fill_opacity_float", default=16)
        pars.add_argument("--stroke_width_float", type=float, dest="stroke_width_float", default=16)
        pars.add_argument("--stroke_opacity_float", type=float, dest="stroke_opacity_float", default=16)

        pars.add_argument("--fill_opacity_cb", type=inkex.Boolean, dest="fill_opacity_cb", default=False)
        pars.add_argument("--stroke_opacity_cb", type=inkex.Boolean, dest="stroke_opacity_cb", default=False)
    
    def effect(self):

        qgis_svg_filepath = self.options.qgis_svg_filepath

        if qgis_svg_filepath is None:
            inkex.errormsg('Please Select a Filename')
            return

        qgis_svg_filepath = qgis_svg_filepath.strip()
        if qgis_svg_filepath == '':
            inkex.errormsg('Please Select a Filename')
            return

        if qgis_svg_filepath[-1] == '/' or qgis_svg_filepath[-1] == '\\':
            inkex.errormsg('Please Select a Filename')
            return

        if self.options.selection_type_radio == 'all':
            elements = self.svg.xpath('//svg:circle | //svg:ellipse | //svg:image | //svg:line| //svg:path |  //svg:polygon | '
                                      '//svg:polyline | //svg:rect | //svg:text | //svg:textPath | //svg:title | '
                                      '//svg:tspan | //svg:use')
        else:
            elements = self.svg.selected

        # For QGIS we need:
        # fill="param(fill) #FFF"
        # stroke="param(outline) #000"
        # stroke-width="param(outline-width) 1"
        # Fill opacity: fill-opacity="param(fill-opacity)"
        # stroke-opacity="param(outline-opacity)"

        fill_color = str(self.options.fill_color)
        stroke_color = str(self.options.stroke_color)
        stroke_width = self.options.stroke_width_float
        fill_opacity = self.options.fill_opacity_float
        stroke_opacity = self.options.stroke_opacity_float
        fill_opacity_bool = self.options.fill_opacity_cb
        stroke_opacity_bool = self.options.stroke_opacity_cb

        for element in elements:

            element_ss = element.specified_style()

            if 'fill' in element_ss.keys():
                qgis_fill = f'param(fill) {fill_color}'
                element.set('fill', qgis_fill)
                if element_ss['fill'].lower() == 'none':
                    element.pop('fill')

            if 'stroke' in element_ss.keys():
                qgis_stroke = f'param(outline) {stroke_color}'
                element.set('stroke', qgis_stroke)
                if element_ss['stroke'].lower() == 'none':
                    element.pop('stroke')

            if 'stroke-width' in element_ss.keys():
                qgis_stroke_width = f'param(outline-width) {stroke_width}'
                element.set('stroke-width', qgis_stroke_width)
                if element_ss['stroke-width'].lower() == 'none':
                    element.pop('stroke-width')

            if stroke_opacity_bool:
                qgis_stroke_opacity = f'param(outline-opacity) {stroke_opacity}'
                element.set('stroke-opacity', qgis_stroke_opacity)

            if fill_opacity_bool:
                qgis_fill_opacity = f'param(fill-opacity) {fill_opacity}'
                element.set('fill-opacity', qgis_fill_opacity)


        for element in elements:
            element.pop('style')

        import os

        if not qgis_svg_filepath.lower().endswith('.svg'):
            qgis_svg_filepath += '.svg'

        try:
            with open(qgis_svg_filepath, 'w') as qgis_svg_file:
                svg_text = etree.tostring(self.svg, pretty_print=True).decode('utf-8')
                qgis_svg_file.write(svg_text)
        except:
            inkex.errormsg('Unable To Write to Filepath')

        sys.exit()

if __name__ == '__main__':
    QgisExport().run()
